/* 
 * Copyright (c) 2018 AT&T Intellectual Property. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.akraino.portal.data;

import java.util.Date;

public class EdgeSite {

	int edgeSiteId;
	String edgeSiteName;
	String inputFile;
	String regionName;
	
	String edgeSiteIP;
	String edgeSiteUser;
	String edgeSitePwd;
	
	Date buildDate;
	
	Date deployDate;
	
	String deployStatus;
	String onapStatus;
	String tempestStatus;
	String vCDNStatus;
	String blueprint;
	
	String outputYaml1;
	String outputYaml2;
	String outputYaml3;
	String outputYaml4;
	String outputYaml5;
	String outputYaml6;
	String outputYaml7;
	String outputYaml8;
	String outputYaml9;
	String outputYaml10;
	String outputYaml11;
	String outputYaml12;
	String outputYaml13;
	String outputYaml14;
	String outputYaml15;
	String outputYaml16;
	String outputYaml17;
	String outputYaml18;
	String outputYaml19;
	String outputYaml20;
	String outputYaml21;
	
	public String getBlueprint() {
		return blueprint;
	}
	public void setBlueprint(String blueprint) {
		this.blueprint = blueprint;
	}
	public Date getBuildDate() {
		return buildDate;
	}
	public void setBuildDate(Date buildDate) {
		this.buildDate = buildDate;
	}
	public Date getDeployDate() {
		return deployDate;
	}
	public void setDeployDate(Date deployDate) {
		this.deployDate = deployDate;
	}
	public int getEdgeSiteId() {
		return edgeSiteId;
	}
	public void setEdgeSiteId(int edgeSiteId) {
		this.edgeSiteId = edgeSiteId;
	}
	public String getEdgeSiteName() {
		return edgeSiteName;
	}
	public void setEdgeSiteName(String edgeSiteName) {
		this.edgeSiteName = edgeSiteName;
	}
	public String getInputFile() {
		return inputFile;
	}
	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}
	public String getOutputYaml1() {
		return outputYaml1;
	}
	public void setOutputYaml1(String outputYaml1) {
		this.outputYaml1 = outputYaml1;
	}
	public String getOutputYaml2() {
		return outputYaml2;
	}
	public void setOutputYaml2(String outputYaml2) {
		this.outputYaml2 = outputYaml2;
	}
	public String getOutputYaml3() {
		return outputYaml3;
	}
	public void setOutputYaml3(String outputYaml3) {
		this.outputYaml3 = outputYaml3;
	}
	public String getOutputYaml4() {
		return outputYaml4;
	}
	public void setOutputYaml4(String outputYaml4) {
		this.outputYaml4 = outputYaml4;
	}
	public String getOutputYaml5() {
		return outputYaml5;
	}
	public void setOutputYaml5(String outputYaml5) {
		this.outputYaml5 = outputYaml5;
	}
	public String getOutputYaml6() {
		return outputYaml6;
	}
	public void setOutputYaml6(String outputYaml6) {
		this.outputYaml6 = outputYaml6;
	}
	public String getOutputYaml7() {
		return outputYaml7;
	}
	public void setOutputYaml7(String outputYaml7) {
		this.outputYaml7 = outputYaml7;
	}
	public String getOutputYaml8() {
		return outputYaml8;
	}
	public void setOutputYaml8(String outputYaml8) {
		this.outputYaml8 = outputYaml8;
	}
	public String getOutputYaml9() {
		return outputYaml9;
	}
	public void setOutputYaml9(String outputYaml9) {
		this.outputYaml9 = outputYaml9;
	}
	public String getOutputYaml10() {
		return outputYaml10;
	}
	public void setOutputYaml10(String outputYaml10) {
		this.outputYaml10 = outputYaml10;
	}
	public String getOutputYaml11() {
		return outputYaml11;
	}
	public void setOutputYaml11(String outputYaml11) {
		this.outputYaml11 = outputYaml11;
	}
	public String getOutputYaml12() {
		return outputYaml12;
	}
	public void setOutputYaml12(String outputYaml12) {
		this.outputYaml12 = outputYaml12;
	}
	public String getOutputYaml13() {
		return outputYaml13;
	}
	public void setOutputYaml13(String outputYaml13) {
		this.outputYaml13 = outputYaml13;
	}
	public String getOutputYaml14() {
		return outputYaml14;
	}
	public void setOutputYaml14(String outputYaml14) {
		this.outputYaml14 = outputYaml14;
	}
	public String getOutputYaml15() {
		return outputYaml15;
	}
	public void setOutputYaml15(String outputYaml15) {
		this.outputYaml15 = outputYaml15;
	}
	public String getOutputYaml16() {
		return outputYaml16;
	}
	public void setOutputYaml16(String outputYaml16) {
		this.outputYaml16 = outputYaml16;
	}
	public String getOutputYaml17() {
		return outputYaml17;
	}
	public void setOutputYaml17(String outputYaml17) {
		this.outputYaml17 = outputYaml17;
	}
	public String getOutputYaml18() {
		return outputYaml18;
	}
	public void setOutputYaml18(String outputYaml18) {
		this.outputYaml18 = outputYaml18;
	}
	public String getOutputYaml19() {
		return outputYaml19;
	}
	public void setOutputYaml19(String outputYaml19) {
		this.outputYaml19 = outputYaml19;
	}
	public String getOutputYaml20() {
		return outputYaml20;
	}
	public void setOutputYaml20(String outputYaml20) {
		this.outputYaml20 = outputYaml20;
	}
	public String getOutputYaml21() {
		return outputYaml21;
	}
	public void setOutputYaml21(String outputYaml21) {
		this.outputYaml21 = outputYaml21;
	}
	public String getEdgeSiteBuildStatus() {
		return edgeSiteBuildStatus;
	}
	public void setEdgeSiteBuildStatus(String edgeSiteBuildStatus) {
		this.edgeSiteBuildStatus = edgeSiteBuildStatus;
	}
	public String getEdgeSiteDeployCreateTarStatus() {
		return edgeSiteDeployCreateTarStatus;
	}
	public void setEdgeSiteDeployCreateTarStatus(String edgeSiteDeployCreateTarStatus) {
		this.edgeSiteDeployCreateTarStatus = edgeSiteDeployCreateTarStatus;
	}
	public String getEdgeSiteDeployGenesisNodeStatus() {
		return edgeSiteDeployGenesisNodeStatus;
	}
	public void setEdgeSiteDeployGenesisNodeStatus(String edgeSiteDeployGenesisNodeStatus) {
		this.edgeSiteDeployGenesisNodeStatus = edgeSiteDeployGenesisNodeStatus;
	}
	public String getEdgeSiteDeployDeployToolStatus() {
		return edgeSiteDeployDeployToolStatus;
	}
	public void setEdgeSiteDeployDeployToolStatus(String edgeSiteDeployDeployToolStatus) {
		this.edgeSiteDeployDeployToolStatus = edgeSiteDeployDeployToolStatus;
	}

	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}



	public String getDeployStatus() {
		return deployStatus;
	}
	public void setDeployStatus(String deployStatus) {
		this.deployStatus = deployStatus;
	}



	public String getOnapStatus() {
		return onapStatus;
	}
	public void setOnapStatus(String onapStatus) {
		this.onapStatus = onapStatus;
	}
	public String getTempestStatus() {
		return tempestStatus;
	}
	public void setTempestStatus(String tempestStatus) {
		this.tempestStatus = tempestStatus;
	}

	public String getvCDNStatus() {
		return vCDNStatus;
	}
	public void setvCDNStatus(String vCDNStatus) {
		this.vCDNStatus = vCDNStatus;
	}

	public String getEdgeSiteIP() {
		return edgeSiteIP;
	}
	public void setEdgeSiteIP(String edgeSiteIP) {
		this.edgeSiteIP = edgeSiteIP;
	}
	public String getEdgeSiteUser() {
		return edgeSiteUser;
	}
	public void setEdgeSiteUser(String edgeSiteUser) {
		this.edgeSiteUser = edgeSiteUser;
	}
	public String getEdgeSitePwd() {
		return edgeSitePwd;
	}
	public void setEdgeSitePwd(String edgeSitePwd) {
		this.edgeSitePwd = edgeSitePwd;
	}



	String edgeSiteBuildStatus;
	String edgeSiteDeployCreateTarStatus;
	String edgeSiteDeployGenesisNodeStatus;
	String edgeSiteDeployDeployToolStatus;
	
	
	
}
